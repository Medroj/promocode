package gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.SwingConstants;

import logic.IPromoCodeLogic;
import start.PromoCodeStart;

import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PromocodeGUIverbessert extends JFrame {
	public PromocodeGUIverbessert(IPromoCodeLogic logic) {
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.NORTH);
		
		JLabel lbl_Überschrift = new JLabel("Promocode-Generierer f\u00FCr 2,5x IT $!");
		lbl_Überschrift.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Überschrift.setFont(new Font("Agency FB", Font.BOLD, 25));
		panel.add(lbl_Überschrift);
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1, BorderLayout.SOUTH);
		
		JButton btn_Beenden = new JButton("Beenden");
		btn_Beenden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		panel_1.add(btn_Beenden);
		
		JPanel panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JLabel lbl_Promocode = new JLabel("");
		lbl_Promocode.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Promocode.setFont(new Font("Tahoma", Font.PLAIN, 40));
		panel_2.add(lbl_Promocode, BorderLayout.CENTER);
		
		JButton btn_PromocodeGenerieren = new JButton("Promocode generieren und speichern");
		btn_PromocodeGenerieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String code = logic.getNewPromoCode();
				lbl_Promocode.setText(code);
			}
		});
		panel_1.add(btn_PromocodeGenerieren);
		setVisible(true);
	}

}
