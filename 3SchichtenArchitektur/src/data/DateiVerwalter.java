package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class DateiVerwalter {
	private File file;
	
	public DateiVerwalter(File file) {
		this.file = file;
	}
	
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader (fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (Exception e) {
			System.out.println("File nicht Vorhanden");
			e.printStackTrace();
		}
	}
	
	public void schreibe(String promo) {
		try {
			FileWriter fw = new FileWriter(this.file, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(promo);
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (Exception e) {
			System.out.println("File nicht Vorhanden");
			e.printStackTrace();
		}
	}
}
